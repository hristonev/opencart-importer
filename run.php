<?php

/*
 * Cli Parameters:
 * debug:source
 */

const BASE_DIR = __DIR__;
chdir(BASE_DIR);

$loader = require BASE_DIR. '/vendor/autoload.php';

use app\Manager;

$app = new Manager();
array_shift($argv);
foreach ($argv as $argument){
    if(strpos($argument, ':') > 0){
        list($key, $value) = explode(':', $argument);
        $app->setParameter($key, $value);
    }
}
$app->run();