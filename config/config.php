<?php

return [
    'base' => [
        'denyWeb' => true,                  						// Deny web server access
        'varDir' => 'var',                  						// Working directory for script
        'logSource' => 'console',           						// Type of status output console|fileName
        'imageDir' => 'var/tmp/img',        						// Directory for download
        'imagePathDB' => 'catalog/image',   						// Path of image in database
        'maxDownloadPerCall' => 60,         						// Download requests per cron call
        'timePerCall' => 300,               						// Cron schedule period
        'refreshData' => 1,                    						// Reload data from source. In seconds default: 86400 (24 hours)
    ],
    'database' => [
        'dbname' => 'ocart',                						// Database name
        'user' => 'root',                   						// Database user
        'password' => '',                   						// Database password
        'host' => 'localhost',              						// Database host
        'driver' => 'pdo_mysql',            						// Database driver
        'tablePrefix' => 'oc_',                                     // Default system table prefix
    ],
    'source' => [
        'method' => 'file',                 						// Source type curl | file
        'filename' => 'var/tmp/xml.php',    						// Source path
        'type' => 'XML',                    						// Source type
        'collectionPointer' => 'root',      						// Root element. Use . for nested pointers.
    ],
    'store' => 0,                                                   // Default store id
    'import' => [
        'brand',
        'category',
        'product'
    ]
];