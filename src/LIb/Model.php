<?php

namespace LIb;


class Model
{
    private $tableName;
    private $fieldSet = [];

    public function __construct($tableName)
    {
        $this->tableName = $tableName;
    }

    public function set($field, $value)
    {
        $this->fieldSet[$field] = $value;
    }
}