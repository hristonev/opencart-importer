<?php

namespace app;

use app\model\Manufacturer;
use Curl\Curl;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use LIb\Model;

class Manager extends Logger
{

    /**
     * @var bool $error stop script on true
     */
    private $error = false;

    /**
     * @var Config $config
     */
    private $config;

    /**
     * @var DriverManager $conn
     */
    private $conn;

    /**
     * @var string $lockFile
     */
    private $lockFile;

    /**
     * @var string
     */
    private $tmpDir = 'tmp';

    private $imageQueue;

    private $imageCompleted;

    private $debug;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
        // parameters
        $this->debug = false;

        $config = new Configuration();
        $this->conn = DriverManager::getConnection(Config::get('database'), $config);

        $this->config = new Config();

        if(Config::get('base.denyWeb') && array_key_exists('HTTP_HOST', $_SERVER)){
            $this->error = true;
            $this->log('Detected web access!');
        }

        if(!is_dir(BASE_DIR. DIRECTORY_SEPARATOR. Config::get('base.varDir'))){
            mkdir(BASE_DIR. DIRECTORY_SEPARATOR. Config::get('base.varDir'));
        }

        $this->tmpDir = BASE_DIR. DIRECTORY_SEPARATOR. Config::get('base.varDir'). DIRECTORY_SEPARATOR. $this->tmpDir;

        if(!is_dir($this->tmpDir)){
            mkdir($this->tmpDir);
        }

        $this->lockFile = $this->tmpDir. DIRECTORY_SEPARATOR. 'app.lock';

        $this->imageQueue = Config::get('base.varDir'). '/'. 'image_pipe_line.json';
        $this->imageCompleted = Config::get('base.varDir'). '/'. 'image_completed.json';

        if(is_file($this->lockFile)){
            $this->error = true;
            $this->log('Already running!');
        }else if(!$this->error){
            file_put_contents($this->lockFile, time());
        }
    }

    public function setParameter($key, $value){
        if(property_exists($this, $key)){
            $this->$key = $value;
            echo "with $key:$value\n";
        }
    }

    public function __destruct()
    {
        if(!$this->error){
            unlink($this->lockFile);
        }
    }

    public function run(){
        if($this->error){
            return;
        }

        $this->init();

        $downloaded = $this->checkDownloadQueue();

        $refreshFile = $this->tmpDir. DIRECTORY_SEPARATOR. 'refresh.lock';
        if(is_file($refreshFile)){
            $lastRefresh = file_get_contents($refreshFile);
        }else{
            $lastRefresh = 0;
        }
        $timePass = time() - $lastRefresh;

        if($downloaded <= 0 && $timePass >= Config::get('base.refreshData')){
            file_put_contents($refreshFile, time());
            ;
            $this->parse(
                $this->getData()
            );
        }

    }

    private function checkDownloadQueue()
    {
        $downloaded = 0;
        $maxItems = Config::get('base.maxDownloadPerCall');
        $maxTime = Config::get('base.timePerCall');
        $itemTimeWindow = ceil($maxTime / $maxItems);
        $maxDownloads = floor($maxTime / $itemTimeWindow);
        $completed = new \stdClass();
        if(is_file($this->imageQueue)){
            $curl = new Curl();
            $queue = json_decode(file_get_contents($this->imageQueue));
            if(is_file($this->imageCompleted)){
                $completed = json_decode(file_get_contents($this->imageCompleted));
            }

            foreach ($queue as $uniqueKey => $queueItem){
                if(!property_exists($completed, $uniqueKey)){
                    $timer = time();
                    $curl->download($queueItem->source, $queueItem->target);
                    $this->log('file saved to '. $queueItem->target);
                    $completed->$uniqueKey = $queueItem->source;
                    $sleepTime = $itemTimeWindow - (time() - $timer);
                    $downloaded++;
                    file_put_contents($this->imageCompleted, json_encode($completed));
                    if($maxDownloads <= $downloaded){
                        return $downloaded;
                    }
                    sleep($sleepTime);
                }
            }
        }

        return $downloaded;
    }

    private function getData()
    {
        $collection = [];
        switch (Config::get('source.method')){
            case 'curl':
                $curl = new Curl();
                $curl->get(Config::get('source.filename'));

                if ($curl->error) {
                    $this->log('Error: ' . $curl->errorCode . ': ' . $curl->errorMessage);
                } else {
                    $collection = $this->loadData($curl->response);
                    $this->log('Parsing '. count($collection). ' products...');
                }
                break;
            case 'file':
                echo Config::get('source.filename');
                $collection = simplexml_load_file(Config::get('source.filename'));
                $this->log('Parsing local file.');
        }

        // Debug source
        if($this->debug === "source"){
            $this->debugSource($collection, 0);
        }

        return $collection;
    }

    /**
     * @param array $source
     * @param integer $level
     */
    private function debugSource(& $source, $level)
    {
        $lastKey = '';
        foreach ($source as $key => $value){
            if($lastKey != $key){
                echo str_repeat("\t", $level);
                echo "$key\n";
                $this->debugSource($value, $level + 1);
            }

            $lastKey = $key;
        }
    }

    private function parse($collection)
    {
        $storeId = (int)Config::get('store');
        $prefix = Config::get('database.tablePrefix');
        $objectConfigCollection = [];
        foreach (Config::get('import') as $object){
            $filename = 'config/model/'. $object. '.json';
            if(is_file($filename)){
                $item = & $objectConfigCollection[];
                $item = json_decode(file_get_contents($filename));
                $item->configFilename = $filename;
            }else{
                $this->log('Config file not found!');
            }
        }
        foreach ($objectConfigCollection as $config) {
            if (!property_exists($config, 'source')) {
                throw new \Exception("Property {source} not found in $config->configFilename");
            }

            $sourceFileds = explode('.', $config->source);
            $objectCollection = & $collection;
            foreach ($sourceFileds as $fieled){
                if (property_exists($objectCollection, $fieled)) {
                    $objectCollection = & $objectCollection->$fieled;
                } elseif (array_key_exists($fieled, $objectCollection)){
                    $objectCollection = & $objectCollection[$fieled];
                }
            }

            if(class_exists($config->model)){
                echo 'manufacturer';die;
            }
//            $model = new Model($prefix. $config->table);
//            foreach ($objectCollection as $object){
//                foreach ($config->fields as $key => $field){
//                    if (property_exists($object, $key)) {
//                        $model->set($key, (string)$object->$key);
//                    }elseif(array_key_exists($key, $object)){
//                        $model->set($key, (string)$object[$key]);
//                    }
//                }
//            }
        }
    }

    /**
     * @param mixed $response
     * @return array
     */
    private function loadData($response)
    {
        $pointers = explode('.', Config::get('source.collectionPointer'));
        foreach ($pointers as $pointer)
        {
            if(array_key_exists($pointer, $response))
            {
                $response = $response[$pointer];
            }
            elseif (property_exists($response, $pointer))
            {
                $response = $response->$pointer;
            }
        }

        return $response;
    }

    /**
     * Initialize
     */
    private function init(){
        $dbConfig = new Configuration();
        $this->conn = DriverManager::getConnection(Config::get('database'), $dbConfig);
    }

}