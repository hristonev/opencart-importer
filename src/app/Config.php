<?php

namespace app;


class Config extends Logger
{
    /**
     * @var string
     */
    private static $data = BASE_DIR. '/config/config.php';

    /**
     * @param $name
     * @return mixed
     */
    public static function get($name)
    {
        $keyParts = explode('.', $name);
        $current = require self::$data;
        foreach ($keyParts as $keyPart){
            if(array_key_exists($keyPart, $current)){
                $current = & $current[$keyPart];
            }
        }

        return $current;
    }
}