<?php

namespace app;


class Logger
{

    /**
     * @param string $message
     */
    public function log($message)
    {
        $config = new Config();
        $logRow = "[". date('Y-m-d H:i:s'). "] ". $message. "\n";
        if($config->get('base.logSource') == 'console'){
            echo $logRow;
        }else{
            $logFileName = BASE_DIR;
            $logFileName .= DIRECTORY_SEPARATOR;
            $logFileName .= $config->get('base.logDir');
            $logFileName .= DIRECTORY_SEPARATOR;
            $logFileName .= $config->get('base.logSource');

            $fp = fopen($logFileName, 'a');
            fwrite($fp, $logRow);
            fclose($fp);
        }
    }

}