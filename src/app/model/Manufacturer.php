<?php

namespace app\model;

class Manufacturer
{
    private $table;
    private $fields = [
        'manufacturer_id' => null,
        'name' => '',
        'image' => '',
        'sort_order' => 0
    ];

    public function __construct($table)
    {

        $this->table = $table;
    }

    public function set($key, $value)
    {
        if(isset($this->fields[$key])){
            $this->fields[$key] = $value;
        }
    }
}