<?php

namespace app\model;


use Doctrine\DBAL\Connection;

class ProductToCategory
{
    private $fields = [
        'product_id' => '',
        'category_id' => ''
    ];
    private $category;
    private $product;
    private $id;

    /**
     * Product constructor.
     * @param Connection $connection
     * @param string $table
     * @param integer $product
     * @param integer $category
     */
    public function __construct(Connection & $connection, $table, $product, $category)
    {
        $this->connection = $connection;
        $this->table = $table;

        $this->fields['product_id'] = $product;
        $this->fields['category_id'] = $category;
        $this->category = $category;
        $this->product = $product;
    }

    public function save()
    {
        foreach ($this->fields as $key => $value){
            $fields[$key] = $value;
        }
        $this->connection->insert($this->table, $this->fields);
        $this->id = $this->connection->lastInsertId();
    }

    public function getId()
    {
        return $this->id;
    }
}