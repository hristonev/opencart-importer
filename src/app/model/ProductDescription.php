<?php

namespace app\model;


use Doctrine\DBAL\Connection;

class ProductDescription
{
    private $fields = [
        'name' => '',
        'description' => '',
        'tag' => '',
        'meta_title' => '',
        'meta_description' => '',
        'meta_keyword' => ''

    ];
    private $product;
    private $id;
    private $languages;

    /**
     * Product constructor.
     * @param Connection $connection
     * @param string $table
     * @param $languages
     * @param $data
     * @param integer $product
     * @internal param $store
     * @internal param int $category
     */
    public function __construct(Connection & $connection, $table, $languages, $data, $product)
    {
        $this->connection = $connection;
        $this->table = $table;

        $this->fields['name'] = (string)$data->name;
        $this->fields['meta_title'] = (string)$data->name;
        $this->product = $product;
        $this->languages = $languages;
    }

    public function insert()
    {
        foreach ($this->languages as $language){
            $this->connection->insert(
                $this->table,
                array_merge([
                        'product_id' => $this->product,
                        'language_id' => $language['language_id']
                    ],
                    $this->fields)
            );
        }
    }

    public function save()
    {
        foreach ($this->languages as $language) {
            $this->connection->update($this->table, $this->fields, [
                'product_id' => $this->product,
                'language_id' => $language['language_id']
            ]);
        }
        $this->id = $this->connection->lastInsertId();
    }

    public function getId()
    {
        return $this->id;
    }
}