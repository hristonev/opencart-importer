<?php

namespace app\model;

use app\Config;
use Doctrine\DBAL\Connection;

class Product
{
    private $fields = [
        'sku' => '',
        'upc' => '',
        'ean' => '',
        'jan' => '',
        'isbn' => '',
        'mpn' => '',
        'location' => '',
        'quantity' => '',
        'stock_status_id' => '',
        'image' => '',
        'manufacturer_id' => 0,
        'shipping' => 0,
        'price' => '',
        'points' => 0,
        'tax_class_id' => 0,
        'date_available' => '1970-01-01',
        'weight' => 0,
        'weight_class_id' => 0,
        'length' => 0,
        'width' => 0,
        'height' => 0,
        'length_class_id' => 0,
        'subtract' => 0,
        'minimum' => 0,
        'sort_order' => 0,
        'status' => 0,
        'viewed' => 0,
        'date_added' => '1970-01-01',
        'date_modified' => '1970-01-01'
    ];

    private $data;
    private $table;
    private $imageURL = null;
    private $imagePath = null;
    private $connection;
    private $model;
    private $isPresent = false;
    private $id = null;

    private $adminOptions = [
        'permanent' => false,
        'photo' => false,
        'price' => false,
        'available' => false
    ];

    /**
     * @param $connection
     * @param $table
     * @param \SimpleXMLElement $data
     * @return Product[]
     */
    public static function buildCollection(& $connection, $table, \SimpleXMLElement $data)
    {
        $collection = [];

        foreach ($data as $item){
            $collection[] = new self($connection, $table, $item);
        }

        return $collection;
    }

    /**
     * Product constructor.
     * @param Connection $connection
     * @param $table
     * @param \SimpleXMLElement $data
     */
    public function __construct(Connection & $connection, $table, \SimpleXMLElement $data)
    {
        $this->connection = $connection;
        $this->table = $table;
        $this->model = (string)$data->serial;
        $product = $this->getProductByModel();
        if($product){
            $this->id = $product['product_id'];
            $this->isPresent = true;
            // set values of presented product
            foreach ($this->fields as $key => $value){
                $this->fields[$key] = $product[$key];
            }
        }else{
            // set image only on new products
            $this->fields['image'] = (string)$data->image;
        }

        $this->data = $data;
        $this->fields['price'] = floatval($data->price) * Config::get('import.product.priceMultiplier');
        $this->fields['quantity'] = ((string)$data->in_stock == 'Y') ? Config::get('import.product.quantityOnStock') : Config::get('import.product.quantityOutStock');
        $this->fields['stock_status_id'] = ((string)$data->in_stock == 'Y') ? Config::get('import.product.statusOnStock') : Config::get('import.product.statusOutStock');

        $this->checkImageForDownload();
    }

    private function checkImageForDownload()
    {
        if(strpos($this->fields['image'], 'http://') !== false || strpos($this->fields['image'], 'https://')){
            $this->imageURL = $this->fields['image'];
            $urlParts = explode('/', $this->fields['image']);
            $imageName = array_pop($urlParts);
            $this->imagePath = Config::get('base.imageDir'). '/'. $imageName;
            $this->fields['image'] = Config::get('base.imagePathDB'). '/'. $imageName;
        }
    }

    public function setAdminOptions(){
        if(!is_null($this->id) && $this->id > 0) {
            $sql = "SELECT * FROM oc_product_update WHERE product_id = :product_id LIMIT 1";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam('product_id', $this->id);
            $stmt->execute();
            $row = $stmt->fetch();
            $this->adminOptions['permanent'] = ((int)$row['permanent'] > 0) ? true : false;
            $this->adminOptions['photo'] = ((int)$row['photo'] > 0) ? true : false;
            $this->adminOptions['price'] = ((int)$row['price'] > 0) ? true : false;
            $this->adminOptions['available'] = ((int)$row['available'] > 0) ? true : false;
        }

    }

    public function save()
    {
        $this->setAdminOptions();

        if(!$this->isPresent){
            $fields = ['model' => $this->model];
            foreach ($this->fields as $key => $value){
                $fields[$key] = $value;
            }
            $this->connection->insert($this->table, $fields);
            $this->id = $this->connection->lastInsertId();
        }elseif(!$this->adminOptions['permanent']){
            if($this->adminOptions['price']){
                unset($this->fields['price']);
            }
            if($this->adminOptions['available']){
                unset($this->fields['quantity']);
                unset($this->fields['stock_status_id']);
            }
            $this->connection->update($this->table, $this->fields, ['model' => $this->model]);
        }
    }

    public function isNew()
    {
        return !$this->isPresent;
    }

    private function getProductByModel()
    {
        $sql = "SELECT * FROM $this->table WHERE model = :model LIMIT 1";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('model', $this->model);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function hasImageURL()
    {
        return !is_null($this->imageURL);
    }

    public function getImageURL()
    {
        return $this->imageURL;
    }

    public function getImagePath()
    {
        return $this->fields['image'];
    }

    public function getImageSavePath()
    {
        return $this->imagePath;
    }

    public function getModel()
    {
        return $this->model;
    }
}