<?php

namespace app\model;


use Doctrine\DBAL\Connection;

class ProductToStore
{
    private $fields = [
        'product_id' => '',
        'store_id' => ''
    ];
    private $product;
    private $id;
    private $store;

    /**
     * Product constructor.
     * @param Connection $connection
     * @param string $table
     * @param integer $product
     * @param $store
     * @internal param int $category
     */
    public function __construct(Connection & $connection, $table, $product, $store)
    {
        $this->connection = $connection;
        $this->table = $table;

        $this->fields['product_id'] = $product;
        $this->fields['store_id'] = $store;
        $this->product = $product;
        $this->store = $store;
    }

    public function save()
    {
        foreach ($this->fields as $key => $value){
            $fields[$key] = $value;
        }
        $this->connection->insert($this->table, $this->fields);
        $this->id = $this->connection->lastInsertId();
    }

    public function getId()
    {
        return $this->id;
    }
}